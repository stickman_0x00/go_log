package log

import "log"

const DEFAULT_FLAGS = log.LstdFlags | log.Lshortfile | log.Lmsgprefix

type config struct {
	log   *log.Logger
	flags int

	text string
	// Font type.
	typ int
	// Font color.
	color int

	// func
	print   *func(v ...any)
	println *func(v ...any)
	printf  *func(format string, v ...any)
}

var levels = map[Level]*config{
	LevelTrace: {
		text:    "TRACE",
		typ:     normal,
		color:   yellow,
		print:   &Trace,
		println: &Traceln,
		printf:  &Tracef,
	},
	LevelDebug: {
		text:    "DEBUG",
		typ:     normal,
		color:   lightCyan,
		print:   &Debug,
		println: &Debugln,
		printf:  &Debugf,
	},
	LevelInfo: {
		text:    "INFO",
		typ:     bold,
		color:   white,
		print:   &Info,
		println: &Infoln,
		printf:  &Infof,
	},
	LevelWarn: {
		text:    "WARN",
		typ:     bold,
		color:   yellow,
		print:   &Warn,
		println: &Warnln,
		printf:  &Warnf,
	},
	LevelError: {
		text:    "ERROR",
		typ:     normal,
		color:   red,
		print:   &Error,
		println: &Errorln,
		printf:  &Errorf,
	},
	LevelFatal: {
		text:    "FATAL",
		typ:     bold,
		color:   red,
		print:   &Fatal,
		println: &Fatalln,
		printf:  &Fatalf,
	},
}
