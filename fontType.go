package log

// reset font type and color
const reset = "\033[0m"

const (
	normal = 0
	bold   = 1
)
