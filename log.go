package log

import (
	"fmt"
	"log"
	"os"
)

var (
	Trace, Debug, Info, Warn, Error, Fatal             func(v ...any)
	Traceln, Debugln, Infoln, Warnln, Errorln, Fatalln func(v ...any)
	Tracef, Debugf, Infof, Warnf, Errorf, Fatalf       func(format string, v ...any)

	level Level = LevelAll
)

func init() {
	set_level()
}

func set_print(level Level, l *config) {
	// Fatal (stop program when called)
	if level == LevelFatal {
		*l.print = l.log.Fatal
		*l.println = l.log.Fatalln
		*l.printf = l.log.Fatalf
		return
	}

	// Default
	*l.print = l.log.Print
	*l.println = l.log.Println
	*l.printf = l.log.Printf

}

func disable_print(l *config) {
	*l.print = func(...any) {}
	*l.println = func(...any) {}
	*l.printf = func(string, ...any) {}
}

func SetLevel(newLevel Level) {
	_, ok := levels[newLevel]
	if !ok {
		return
	}

	level = newLevel
	set_level()
}

func set_level() {
	for currentLevel, l := range levels {
		if currentLevel < level {
			// Disable print
			disable_print(l)
			continue
		}

		text := fmt.Sprintf("\033[%d;%dm[%s]%s ",
			l.typ,
			l.color,
			l.text,
			reset,
		)

		// populate flags
		l.flags = DEFAULT_FLAGS

		// Create log
		l.log = log.New(os.Stdout, text, l.flags)

		set_print(currentLevel, l)
	}
}
